class customer{
    constructor(first,last,email,contact,address){
        this.fname = first;
        this.lname = last;
        this.mailid = email;
        this.contact=contact;
        this.address=address;
    }
}
function execute()
{
    let first=document.forms["practical3"]["fname"].value;
    let last=document.forms["practical3"]["lname"].value;
    let email=document.forms["practical3"]["email"].value;
    let contact=document.forms["practical3"]["phone"].value;
    let address=document.forms["practical3"]["address"].value;
    let f = /\d/.test(first);
    let l = /\d/.test(last);
    let spacef = /\s/.test(first);
    let spacel =/\s/.test(last);
    let characterphone = /[a-z]/i.test(contact);
    if(f==true)
    {
        alert("ONLY ALPHABETS ALLOWED IN FIRST NAME");
        return;
    }
    if(l==true)
    {
        alert("ONLY ALPHABETS ALLOWED IN LAST NAME");
        return;
    }
    if(spacef==true || spacel)
    {
        alert("NO SPACES ALLOWED");
        return;
    }
    if(characterphone==true || characterphone.length >10 || characterphone.length <10 )
    {
        alert("INVALID PHONE NUMBER");
        return;
    }
    let c1 = new customer(first,last,email,contact,address);
    document.cookie="firstname="+first;
    if(localStorage.length == 0)
    {
        localStorage["Records"]=JSON.stringify(new Array());
        let stored_datas = JSON.parse(localStorage["Records"]);
        stored_datas.push(c1);
        localStorage["Records"]=JSON.stringify(stored_datas);
    }
    else if(localStorage.length!=0)
    {
        let stored_datas = JSON.parse(localStorage["Records"]);
        stored_datas.push(c1);
        localStorage["Records"]=JSON.stringify(stored_datas);
    }
}
function displaytable(){
    let capacity = localStorage["Records"].length;
    let stored_datas = JSON.parse(localStorage["Records"]);
    for(let i=0;i<capacity;i++){
        const maintable = document.getElementById("datatable");
        const tablerow = document.createElement("tr");
        tablerow.className += "row";
        const first_cell = document.createElement("td");
        let firstname = document.createTextNode(stored_datas[i]["fname"]);
        first_cell.appendChild(firstname);
        tablerow.appendChild(first_cell);
        const last_cell = document.createElement("td");
        let lastname = document.createTextNode(stored_datas[i]["lname"]);
        last_cell.appendChild(lastname);
        tablerow.appendChild(last_cell);
        const complete_cell = document.createElement("td");
        const complete = document.createTextNode(stored_datas[i]["fname"]+" "+stored_datas[i]["lname"]);
        complete_cell.appendChild(complete);
        tablerow.appendChild(complete_cell);
        const mail_cell = document.createElement("td");
        const mailid = document.createTextNode(stored_datas[i]["mailid"]);
        mail_cell.appendChild(mailid);
        tablerow.appendChild(mail_cell);
        const phone_cell = document.createElement("td");
        const phone= document.createTextNode(stored_datas[i]["contact"]);
        phone_cell.appendChild(phone);
        tablerow.appendChild(phone_cell);
        const add=document.createElement("td");
        const address=document.createTextNode(stored_datas[i]["address"]);
        add.appendChild(address);
        tablerow.appendChild(add);
        maintable.appendChild(tablerow);
    }
}